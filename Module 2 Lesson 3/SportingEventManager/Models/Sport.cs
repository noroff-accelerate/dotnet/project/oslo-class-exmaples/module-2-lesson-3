﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SportingEventManager.Models
{
    public class Sport
    {
        public int SportID { get; set; } // PK
        public string Name { get; set; }
    }
}
