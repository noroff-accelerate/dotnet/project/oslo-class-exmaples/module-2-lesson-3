﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SportingEventManager.Models
{
    public class Person
    {
        public string Name { get; set; }
        public DateTime DOB { get; set; }
        public string Gender { get; set; }
    }
}
