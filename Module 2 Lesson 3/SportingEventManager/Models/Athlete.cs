﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SportingEventManager.Models
{
    public class Athlete : Person
    {
        public int AthleteID { get; set; } // PK
        public int CoachID { get; set; } // FK
        public int SportID { get; set; } // FK
    }
}
