﻿using SportingEventManager.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace SportingEventManager.Helpers
{
    // It is prefered to make new connections on every interaction,
    // opening one connection and keeping it open is very memory intensive.
    public class DBHelper
    {
        private static SqlConnectionStringBuilder builder;

        #region Testing connection
        /// <summary>
        /// Tests the connection to the specified SQL Server database.
        /// </summary>
        public static void TestConnection()
        {
            // Build connection string
            builder = new SqlConnectionStringBuilder
            {
                DataSource = "MININT-07CHMFP\\SQLEXPRESS",
                InitialCatalog = "SportsMan",
                IntegratedSecurity = true
            };

            // Connect to the SQLServer instance
            Console.Write("Testing connecting to SQL Server ... \n");

            try
            {
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();
                    Console.WriteLine("Test was successful. \n ------------------------------------");
                }
            }
            catch (Exception ex)
            {
                HandleError(ex);
            }
        }
        #endregion

        #region Seeding
        /// <summary>
        /// Checks to see if the database has any data, if it doesnt, seed new data. This is not the best way to do seeding, just for this exmaple it was suitable.
        /// </summary>
        internal static void SeedIfEmpty()
        {
            // Select top 1 is fastest performing way to check if a table has records.
            string sql = "SELECT TOP 1 SportId FROM Sport";
            try
            {
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            // If the reader can .Read() there are records
                            if (!reader.Read())
                            {
                                // When there are no records, we seed.
                                SeedData();
                            }
                        }
                    }
                }
            } catch(Exception ex)
            {
                HandleError(ex);
            }
        }

        /// <summary>
        /// Seeds data into the database if it does not exist.
        /// </summary>
        private static void SeedData()
        {
            Console.WriteLine("\nSeeding database...");

            #region Seed sports
            // Mock up some Sports to use as seeded data
            List<Sport> sports = new List<Sport>()
            {
                new Sport(){ SportID=1, Name="Basketball" },
                new Sport(){ SportID=2, Name="Running" },
                new Sport(){ SportID=3, Name="Football" },
                new Sport(){ SportID=4, Name="Skiing" },
                new Sport(){ SportID=5, Name="Curling" }
            };
            // Insert the seeded Sports into the DB
            InsertSeedSports(sports);
            #endregion

            #region Seed coaches
            // Mock up some Coaches to use as seeded data
            List<Coach> coaches = new List<Coach>()
            {
                new Coach(){ CoachID=1, Name = "John McIntyre", DOB=DateTime.Now.AddYears(-40), Gender="Male"},
                new Coach(){ CoachID=2, Name = "Renate Blindheim", DOB=DateTime.Now.AddYears(-30), Gender="Female"},
                new Coach(){ CoachID=3, Name = "Phil Jackson", DOB=DateTime.Now.AddYears(-74), Gender="Male"},
                new Coach(){ CoachID=4, Name = "Christine Girard", DOB=DateTime.Now.AddYears(-35), Gender="Female"},
            };
            // Insert the seeded Coaches into the DB
            InsertSeedCoaches(coaches);
            #endregion

            #region Seed athletes
            // Mock up some Athletes to use as seeded data
            List<Athlete> athletes = new List<Athlete>() 
            { 
                new Athlete(){ AthleteID=1, Name="Micheal Jordan", SportID=1, CoachID=3, Gender="Male", DOB= new DateTime(1963,2,17)},
                new Athlete(){ AthleteID=2, Name="Thomas Ulsrud", SportID=5, CoachID=4, Gender="Male", DOB= new DateTime(1971,10,21)},
                new Athlete(){ AthleteID=3, Name="Kjetil Andre Aamodt", SportID=4, CoachID=2, Gender="Male", DOB= new DateTime(1971,9,2)},
                new Athlete(){ AthleteID=4, Name="David Beckham", SportID=3, CoachID=1, Gender="Male", DOB= new DateTime(1975,5,2)},
                new Athlete(){ AthleteID=5, Name="Usain Bolt", SportID=2, CoachID=1, Gender="Male", DOB= new DateTime(1986,08,21)}
            };
            // Insert the seeded Athletes into the DB
            InsertSeedAthletes(athletes);
            #endregion
        }

        #region Insertion methods for seeding
        /// <summary>
        /// Inserts a list of Sports into the DB with the provided connection string.
        /// </summary>
        /// <param name="sports">The list of sports to be seeded into the database</param>
        private static void InsertSeedSports(List<Sport> sports)
        {
            Console.WriteLine("\nInserting example Sports...");
            foreach(Sport sport in sports)
            {
                // Run SQL Insert for each seeded sport. Catch any errors
                try
                {
                    using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                    {
                        connection.Open();
                        string sql = "SET IDENTITY_INSERT Sport ON " +
                            "INSERT INTO Sport(SportId,Name) VALUES(@ID,@Name) " +
                            "SET IDENTITY_INSERT Sport OFF ";

                        using (SqlCommand command = new SqlCommand(sql, connection))
                        {
                            command.Parameters.AddWithValue("@ID", sport.SportID);
                            command.Parameters.AddWithValue("@Name", sport.Name);

                            command.ExecuteNonQuery();
                        }
                    }
                } catch(Exception ex)
                {
                    HandleError(ex);
                }
            }
        }
        /// <summary>
        /// Inserts a list of Coaches into the DB with the provided connection string.
        /// </summary>
        /// <param name="coaches">The list of coaches to be seeded into the the database</param>
        private static void InsertSeedCoaches(List<Coach> coaches)
        {
            Console.WriteLine("\nInserting example Coaches...");
            foreach(Coach coach in coaches)
            {
                // Run SQL Insert for each seeded coach.
                try
                {
                    using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                    {
                        connection.Open();
                        string sql = "SET IDENTITY_INSERT Coach ON " +
                            "INSERT INTO Coach(CoachId,Name,DOB,Gender) VALUES(@ID,@Name,@DOB,@Gender) " +
                            "SET IDENTITY_INSERT Coach OFF";

                        using (SqlCommand command = new SqlCommand(sql, connection))
                        {
                            command.Parameters.AddWithValue("@ID", coach.CoachID);
                            command.Parameters.AddWithValue("@Name", coach.Name);
                            command.Parameters.AddWithValue("@DOB", coach.DOB);
                            command.Parameters.AddWithValue("@Gender", coach.Gender);

                            command.ExecuteNonQuery();
                        }
                    }
                }
                catch (Exception ex)
                {
                    HandleError(ex);
                }
            }
        }

        /// <summary>
        /// Inserts a list of Athletes into the DB with the provided connection string.
        /// </summary>
        /// <param name="athletes">The list of athletes to be seeded into the database</param>
        private static void InsertSeedAthletes(List<Athlete> athletes)
        {
            Console.WriteLine("\nInserting example Athletes...");
            foreach(Athlete athlete in athletes)
            {
                // Run SQL Insert for each seeded athlete.
                try
                {
                    using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                    {
                        connection.Open();
                        string sql = "SET IDENTITY_INSERT Athlete ON " +
                            "INSERT INTO Athlete(AthleteId,Name,DOB,Gender,CoachId,SportId) VALUES(@ID,@Name,@DOB,@Gender,@Coach,@Sport) " +
                            "SET IDENTITY_INSERT Athlete OFF";

                        using (SqlCommand command = new SqlCommand(sql, connection))
                        {
                            command.Parameters.AddWithValue("@ID", athlete.AthleteID);
                            command.Parameters.AddWithValue("@Name", athlete.Name);
                            command.Parameters.AddWithValue("@DOB", athlete.DOB);
                            command.Parameters.AddWithValue("@Gender", athlete.Gender);
                            command.Parameters.AddWithValue("@Coach", athlete.CoachID);
                            command.Parameters.AddWithValue("@Sport", athlete.SportID);

                            command.ExecuteNonQuery();
                        }
                    }
                }
                catch (Exception ex)
                {
                    HandleError(ex);
                }
            }
        }
        #endregion

        #endregion

        #region DB CRUD
        /// <summary>
        /// Queries all the athlete data from the database and writes it to the console for viewing.
        /// </summary>
        internal static void ShowAllAthletes()
        {
            Console.WriteLine("\nShowing all athlete data...");

            string sql = "SELECT a.Name AS Athlete, a.DOB AS Born, a.Gender, " +
                "c.Name AS Coach, s.Name AS Sport FROM Athlete AS a " +
                "INNER JOIN Coach AS c ON a.CoachId = c.CoachId " +
                "INNER JOIN Sport AS s ON a.SportId = s.SportId;";
            try
            {
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            DisplayAthletes(reader);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                HandleError(ex);
            }
        }
        /// <summary>
        /// Adds an athlete to the database.
        /// </summary>
        /// <param name="athlete">The athlete object to add</param>
        internal static void AddAthlete(Athlete athlete)
        {
            string sql = "INSERT INTO Athlete(Name,DOB,Gender,CoachId,SportId) " +
                "VALUES(@Name,@DOB,@Gender,@Coach,@Sport)";
            try
            {
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@Name", athlete.Name);
                        command.Parameters.AddWithValue("@DOB", athlete.DOB);
                        command.Parameters.AddWithValue("@Gender", athlete.Gender);
                        command.Parameters.AddWithValue("@Coach", athlete.CoachID);
                        command.Parameters.AddWithValue("@Sport", athlete.SportID);

                        command.ExecuteNonQuery();
                    }
                }
            } catch(Exception ex)
            {
                HandleError(ex);
            }
        }
        /// <summary>
        /// Updates an athlete in the database with the values provided
        /// </summary>
        /// <param name="athlete">The athlete object with the updated information</param>
        internal static void UpdateAthlete(Athlete athlete)
        {
            string sql = "UPDATE Athlete SET " +
                "Name=@Name, Gender=@Gender, DOB=@DOB, SportId=@Sport, CoachId=@Coach " +
                "WHERE AthleteId=@Athlete";
            try
            {
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@Athlete", athlete.AthleteID);
                        command.Parameters.AddWithValue("@Name", athlete.Name);
                        command.Parameters.AddWithValue("@Gender", athlete.Gender);
                        command.Parameters.AddWithValue("@DOB", athlete.DOB);
                        command.Parameters.AddWithValue("@Sport", athlete.SportID);
                        command.Parameters.AddWithValue("@Coach", athlete.CoachID);
                    }
                }
            } catch(Exception ex)
            {
                HandleError(ex);
            }
        }
        /// <summary>
        /// Deletes an athlete in the database.
        /// </summary>
        /// <param name="AthleteID">The Id of the athlete to be deleted</param>
        internal static void DeleteAthlete(int AthleteID)
        {
            string sql = "DELETE FROM Athlete WHERE AthleteId=@Athlete";
            try
            {
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@Athlete", AthleteID);

                        command.ExecuteNonQuery();
                    }
                }
            } catch(Exception ex)
            {
                HandleError(ex);
            }
        }

        /// <summary>
        /// Displays the relevent athletes information to the console window.
        /// </summary>
        /// <param name="athleteId">The Id of the athlete that needs to be displayed</param>
        internal static void FetchAthlete(int athleteId)
        {
            string sql = "SELECT a.Name AS Athlete, a.DOB AS Born, a.Gender, " +
                "c.Name AS Coach, s.Name AS Sport FROM Athlete AS a " +
                "INNER JOIN Coach AS c ON a.CoachId = c.CoachId " +
                "INNER JOIN Sport AS s ON a.SportId = s.SportId " +
                "WHERE a.AthleteId=@Athlete;";
            try
            {
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@Athlete", athleteId);

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            DisplayAthletes(reader);
                        }
                    }
                }
            } catch(Exception ex)
            {
                HandleError(ex);
            }
        }
        #endregion

        /// <summary>
        /// Prints an exception the the console and terminates the application after the user pressed enter.
        /// </summary>
        /// <param name="ex">The exception which is going to be printed to the console</param>
        private static void HandleError(Exception ex)
        {
            Console.WriteLine("\nSomething went wrong: " + ex.Message);
            Console.WriteLine("\nPress enter to close the application...");
            Console.ReadLine();
            Environment.Exit(0);
        }
        /// <summary>
        /// This method writes a SqlDataReader's athlete data to the console
        /// </summary>
        /// <param name="reader">The reader populated with athletes after command.ExecuteReader() has been called</param>
        private static void DisplayAthletes(SqlDataReader reader)
        {
            // Write headings
            Console.WriteLine("\nAthlete information\n");
            Console.WriteLine($"{reader.GetName(0)} \t\t {reader.GetName(1)} \t\t " +
                $"{reader.GetName(2)} \t {reader.GetName(3)} \t\t\t {reader.GetName(4)}");
            // Write data
            while (reader.Read())
            {
                Console.WriteLine($"{reader.GetString(0)} \t\t {reader.GetDateTime(1).ToShortDateString()} \t " +
                $"{reader.GetString(2)} \t\t {reader.GetString(3)} \t\t {reader.GetString(4)}");
            }
            Console.WriteLine();
        }
    }
}
