﻿using SportingEventManager.Helpers;
using SportingEventManager.Models;
using System;

namespace SportingEventManager
{
    class Program
    {
        static void Main(string[] args)
        {
            // Test the connection to the DB and get the connection string.
            DBHelper.TestConnection();

            // Seed the database if there is no data.
            DBHelper.SeedIfEmpty();

            // Show the database data
            DBHelper.ShowAllAthletes();

            #region Simple CRUD for Athlete
            // Add an exmaple athlete
            Athlete exmapleAthlete = new Athlete()
            {
                Name = "Nicholas Lennox", CoachID = 1, DOB = DateTime.Today, Gender = "Male", SportID = 1
            };

            DBHelper.AddAthlete(exmapleAthlete);

            // Show the database data after the add. 
            DBHelper.ShowAllAthletes();

            // There are methods for Update and Delete in DBHelper
            #endregion

        }
    }
}
