# Sport management system proof of concept

<img src="https://www.noroff.no/images/docs/vp2018/Noroff-logo_STDM_vertikal_RGB.jpg" alt="banner" width="450"/>

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)
> A example project using System.Data.SqlClient to do basic CRUD in a sporting management system.

## Table of Contents

- [Introduction](#introduction)
- [Getting started](#gettingstarted)
- [Prerequisites](#prerequisites)
- [Maintainers](#maintainers)
- [License](#license)

## Introduction

This application was created in line with the specifications listed in `Specs.docx`. The application is written in C# using .NET Core. It is a console application that makes use of the System.Data.SqlClient package to connect to a SQL Server database instance and manipulate its data. 

## Getting started
- Clone the repository
- Run `Database.sql` to generate the database
- Build the application
- Run the application

## Prerequisites
- .NET Core Framework
- Visual Studio 2017/2019
- Sql Server Management Studio

## Maintainers

[Nicholas Lennox (@NicholasLennox)](https://gitlab.com/NicholasLennox)

## Contributing

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

All rights reserved © 2019-2020 Noroff Accelerate
